import {ACTION_TYPES} from './../constants/index'
import moment from 'moment'

let now = moment()

export function defaultDate(state = now.clone(), action) {
    switch (action.type) {
        case ACTION_TYPES.ADD_DATE:
            let newDate = state
            newDate = action.defaultDate
            return newDate
        default: return state
    }
}