import {ACTION_TYPES} from './../constants/index'

export function events(state = [], action) {
    switch (action.type) {
        case ACTION_TYPES.ADD_EVENT:
            return [
                {
                    id: state.reduce((maxId, event) => Math.max(event.id, maxId), -1) + 1,
                    date: action.date,
                    text: action.text
                },
                ...state
            ]
        case ACTION_TYPES.DELETE_EVENT:
            return state.filter(event =>
                event.id !== action.id
            )
        case ACTION_TYPES.EDIT_EVENT:
            return state.map(event =>
                event.id === action.id ? { ...event, text: action.text, date: action.date } : event
            )
        default: return state
    }
}