import {combineReducers} from 'redux'
import {events} from './events'
import {defaultDate} from './defaultDate'

const rootReducer = combineReducers({
    events,
    defaultDate
})

export default rootReducer