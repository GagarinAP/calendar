import React from 'react'
import ReactDOM from 'react-dom'
import {Provider} from 'react-redux'
import store from './store/index'
import {
    BrowserRouter as Router,
    Route,
    Switch
} from 'react-router-dom'

import Event from './components/Event'
import Calendar from './components/Calendar'
import Year from './components/Year'
import Month from './components/Month'
import Day from './components/Day'

ReactDOM.render(
    <Provider store={store}>
        <Router>
            <div>
                <Switch>
                    <Route exact component={Calendar}
                        path='/'
                    />
                    <Route component={Event}
                        path='/event'
                    />
                    <Route component={Year}
                        path='/year'
                    />
                    <Route component={Month}
                        path='/month'
                    />
                    <Route component={Day}
                        path='/day'
                    />
                </Switch>
            </div>
        </Router>
    </Provider>,
    document.getElementById('root')
)