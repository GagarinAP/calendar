import rootReducer from './../reducers/index.js'
import {createStore, applyMiddleware} from 'redux'
import {createLogger} from 'redux-logger'
import thunk from 'redux-thunk'
import { loadState, saveState } from './localStorage'

const logger = createLogger()
const persistedState = loadState()

const store = createStore(
    rootReducer,
    persistedState,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    applyMiddleware(
        thunk,
        logger
    )
)

store.subscribe(() => {
    saveState({
        events: store.getState().events,
        defaultDate: store.getState().defaultDate
    })
})

export default store