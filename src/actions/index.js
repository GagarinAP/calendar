import {ACTION_TYPES} from './../constants/index'
import store from './../store/index'

export function addEvent (text,date) {
    store.dispatch({
        type: ACTION_TYPES.ADD_EVENT,
        text: text,
        date: date
    })
}

export function editEvent (id,text,date) {
    store.dispatch({
        type: ACTION_TYPES.EDIT_EVENT,
        id: id,
        text: text,
        date: date
    })
}

export function deleteEvent (id) {
    store.dispatch({
        type: ACTION_TYPES.DELETE_EVENT,
        id: id
    })
}

export function defaultDate (date) {
    store.dispatch({
        type: ACTION_TYPES.ADD_DATE,
        defaultDate: date
    })
}
