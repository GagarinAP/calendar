import React from 'react'
import * as ACTIONS from '../actions/index'
import {withRouter} from 'react-router-dom'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import moment from 'moment'
import _ from 'lodash'
import Nav from './Nav'
import uuidv4 from 'uuid/v4'
import { A, Ul, LiCenter } from './styles/MonthStyles'

class Month extends React.Component{
    constructor(props) {
        super(props)

        this.state = {
            defaultDate: moment(this.props.defaultDate),
            events: this.props.events
        }

        this.handleChangeDown = this.handleChangeDown.bind(this)
        this.handleChangeUp = this.handleChangeUp.bind(this)
        this.arrayDayInMonth = this.arrayDayInMonth.bind(this)
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.defaultDate !== this.props.defaultDate) {
            this.setState({
                defaultDate: nextProps.defaultDate
            })
        }
    }

    handleChangeDown() {
        const { defaultDate } = this.state

        this.setState({
            defaultDate: defaultDate.subtract(1, 'months')
        })

        ACTIONS.defaultDate(defaultDate)
    }

    handleChangeUp() {
        const { defaultDate } = this.state

        this.setState({
            defaultDate: defaultDate.add(1, 'months')
        })

        ACTIONS.defaultDate(defaultDate)
    }

    arrayDayInMonth(defaultDate, events) {
        const days = []
        const begin = defaultDate.clone().startOf('month').startOf('week')
        const dateNow = moment().clone()

        let result = _.filter(events, (todo) => {
            return moment(todo.date).clone().format('MMYYYY') === defaultDate.format('MMYYYY')
        });

        const StartMonth = defaultDate.clone().startOf('month').startOf('week').format('D')
        const DayInPreMonth = moment(defaultDate).subtract(1, 'month').clone().daysInMonth()
        const forIterator = parseInt(DayInPreMonth,10) - parseInt(StartMonth,10)

        if(parseInt(begin.format('D'),10) < parseInt(forIterator,10)+1) {
            for (let i = 1; i <= parseInt(forIterator, 10) + 1; i++) {
                let haveEvent = result.filter(t => parseInt(moment(t.date).format('D'),10) === i)
                days.push(
                    <li key={uuidv4()}
                        style={
                            haveEvent.length > 0 ? {borderRadius:'10%',backgroundColor: '#fff'} : {}
                        }
                    >
                        {
                            dateNow.format('DDMMYYYY') === begin.format('DDMMYYYY')
                                ?
                                <span style={{color:'green'}}>{i}</span>
                                :
                                i
                        }
                    </li>
                )
                begin.add(1, 'days')
            }
        } else {
            for (let i = 1; i <= parseInt(forIterator, 10) + 1; i++) {
                days.push(<li key={uuidv4()}></li>)
                begin.add(1, 'days')
            }
            for (let i = 1; i <= parseInt(moment(defaultDate).clone().daysInMonth(), 10); i++) {

                let haveEvent = result.filter(t => parseInt(moment(t.date).format('D'),10) === i)

                days.push(
                    <li key={i}
                        style={
                            haveEvent.length > 0 ? {borderRadius:'10%',backgroundColor: '#fff'} : {}
                        }
                    >
                        {
                            dateNow.format('DDMMYYYY') === begin.format('DDMMYYYY')
                                ?
                                <span style={{color:'green'}}>{i}</span>
                                :
                                i
                        }
                    </li>
                )
                begin.add(1, 'days')
            }
        }
        return days
    }

    render(){
        const { defaultDate, events } = this.state

        return(
            <div className="container">
                <div className="row">

                    <Nav defaultDate={defaultDate.format('DD MMMM YYYY')} />

                    <div className="col-md-8 col-md-offset-2">
                        <div className="month">
                            <Ul>
                                <li className="prev">
                                    <A type="button" className="btn btn-link" onClick={this.handleChangeDown}>
                                        <i className="fa fa-angle-left" />
                                    </A>
                                </li>
                                <li className="next">
                                    <A type="button" className="btn btn-link" onClick={this.handleChangeUp}>
                                        <i className="fa fa-angle-right" />
                                    </A>
                                </li>
                                <LiCenter>
                                    <span>{defaultDate.format("MMMM YYYY")}</span>
                                </LiCenter>
                            </Ul>
                        </div>

                        <ul className="weekdays">
                            {_.map(moment()._locale._weekdaysShort,(value,index) => {
                                return <li className="weekdays-li" key={index}>{value}</li>
                            })}
                        </ul>

                        <ul className="days">
                            {
                                this.arrayDayInMonth(defaultDate, events)
                            }
                        </ul>

                    </div>

                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    events: state.events,
    defaultDate: state.defaultDate
})

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators(ACTIONS, dispatch)
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Month))