import React from 'react'
import {withRouter} from 'react-router-dom'
import Nav from './Nav'
import { Div } from './styles/CalendarStyles'

const Calendar = () => {
    return(
        <div className="container">
            <div className="row">
                <Nav />
                <Div className="col-md-12">
                    <div className="jumbotron">
                        <h1 className="text-center">Calendar Event`s</h1>
                        <h2 className="text-center">How to:</h2>
                        <ul className="list-group">
                            <li className="list-group-item">
                                <h3><strong>1. Event page:</strong> add you event(s)</h3>
                            </li>
                            <li className="list-group-item">
                                <h3><strong>2. Year page:</strong> route you event year(s)</h3>
                            </li>
                            <li className="list-group-item">
                                <h3><strong>3. Month page:</strong> route you event month(s), show you event(s)</h3>
                            </li>
                            <li className="list-group-item">
                                <h3><strong>4. Day page:</strong> route you event day(s) and watch time-event</h3>
                            </li>
                            <li className="list-group-item">
                                <h3><strong>5. Default link:</strong> show default date(date now)</h3>
                            </li>
                        </ul>
                    </div>
                </Div>
            </div>
        </div>
    )
}

export default withRouter(Calendar)