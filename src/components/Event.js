import React from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import * as ACTIONS from '../actions/index'
import _ from 'lodash'
import moment from 'moment'
import Nav from './Nav'
import { Form, Div, Label, PBody, BtnGroup, NForm } from './styles/EventStyles'

class App extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            events: this.props.events,
            text: '',
            date: '',
            editing: null,
            newText: '',
            newDate: ''
        }

        this.handleTextInput = this.handleTextInput.bind(this)
        this.handleDateInput = this.handleDateInput.bind(this)
        this.handlePopupTextInput = this.handlePopupTextInput.bind(this)
        this.handlePopupDateInput = this.handlePopupDateInput.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.toggleEditing = this.toggleEditing.bind(this)
        this.renderItem = this.renderItem.bind(this)
        this.handleUpdate = this.handleUpdate.bind(this)
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.events !== this.props.events) {
            this.setState({
                events: nextProps.events
            })
        }
    }

    handleTextInput(event) {
        this.setState({
            text: event.target.value
        })
    }

    handleDateInput(event) {
        this.setState({
            date: event.target.value
        })
    }

    handlePopupTextInput(event) {
        this.setState({
            newText: event.target.value
        })
    }

    handlePopupDateInput(event) {
        this.setState({
            newDate: event.target.value
        })
    }

    handleSubmit(event) {
        const { text, date } = this.state

        if(text.length === 0 || date.length === 0) return null

        ACTIONS.addEvent(text, date)

        this.setState({
            text: '',
            date: ''
        })

        event.preventDefault()
    }

    toggleEditing(itemId,itemText,itemDate) {
        this.setState({
            editing: itemId,
            newText: itemText,
            newDate: itemDate
        })
    }

    handleUpdate(event) {
        const { editing, newText, newDate } = this.state

        ACTIONS.editEvent(editing, newText, newDate)

        this.setState({
            editing: null,
            newText: '',
            newDate: ''
        })
    }

    renderItem(value) {
        const { editing, newText, newDate } = this.state

        if ( editing === value.id ) {
            return <li key={value.id}
                       className="list-group-item"
                       style={{marginBottom: '15px', borderRadius: '4px', padding: '5px 15px'}}>
                <div className="row">
                    <NForm className="col-md-12">
                    <form className="form-inline" onSubmit={this.handleUpdate}>
                        <div className="form-group">
                            <label htmlFor="text">Text:</label>
                            <input value={newText} name="text" type="text" className="form-control" id="text"
                                   onChange={this.handlePopupTextInput}
                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="date">Date:</label>
                            <input value={newDate} name="date" type="datetime-local" className="form-control"
                                   id="date" onChange={this.handlePopupDateInput}
                            />
                        </div>
                        <input type="submit" className="btn btn-primary" value="Update" />
                    </form>
                    </NForm>
                </div>
            </li>
        } else {
            return <div key={value.id} className="panel panel-primary">
                <div className="panel-heading">
                    {moment(value.date).format('DD MMMM YYYY HH:mm')}
                    <BtnGroup className="btn-group pull-right">
                        <button onClick={() => ACTIONS.deleteEvent(value.id)} className="btn btn-danger" type="button">
                            Delete
                        </button>
                        <button onClick={() => this.toggleEditing(value.id,value.text,value.date)}
                                className="btn btn-success" type="button"
                        >
                            Edit
                        </button>
                    </BtnGroup>
                </div>
                <PBody className="panel-body">{value.text}</PBody>
            </div>
        }
    }

    render() {
        const { events, text, date } = this.state

        return(
            <div className="container">
                <div className="row">

                    <Nav />

                    <Div className="col-md-8 col-md-offset-2">
                        <div className="panel panel-default">
                            <Form className="form-inline" onSubmit={this.handleSubmit}>
                                <div className="form-group">
                                    <Label htmlFor="text" className="control-label">Text:</Label>
                                    <input value={text} name="text" type="text" className="form-control" id="text"
                                           onChange={this.handleTextInput}
                                    />
                                </div>
                                <div className="form-group">
                                    <Label htmlFor="date" className="control-label">Date:</Label>
                                    <input value={date} name="date" type="datetime-local" className="form-control"
                                           id="date" onChange={this.handleDateInput}
                                    />
                                </div>
                                <div className="form-group">
                                    <input type="submit" className="btn btn-success" value="Add" />
                                </div>
                            </Form>
                        </div>
                    </Div>

                    <div className="col-md-8 col-md-offset-2">
                        <div className="panel-group">
                            {
                                _.map(events,value =>
                                    this.renderItem(value)
                                )
                            }
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    events: state.events
})

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators(ACTIONS, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(App)