import styled from 'styled-components'

const Div = styled.div`
	background-color: yellow;
	font-size: 20px;
`;

const A = styled.a`
	color: white;
	font-size: 30px;
	padding: 0;
`;

const LiCenter = styled.li`
	text-align: center;
	& > span {
	    font-size: 30px;
	}
`;

const Ul = styled.ul`
	list-style: none;
`;

export { Div, A, LiCenter, Ul };