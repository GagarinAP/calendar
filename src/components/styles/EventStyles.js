import styled from 'styled-components'

const Form = styled.form`
	padding: 20px;
`;

const Div = styled.div`
	padding-bottom: 0px;
`;

const Label = styled.label`
	font-size: 20px;
`;

const PBody = styled.label`
	font-size: 24px;
`;

const BtnGroup = styled.div`
	top: -7px;
`;

const NForm = styled.div`
	padding: 20px;
`;

export { Form, Div, Label, PBody, BtnGroup, NForm };