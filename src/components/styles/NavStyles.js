import styled from 'styled-components'

const Container = styled.div`
	padding: 20px 0px;
`;

const P = styled.p`
	font-size: 20px;
`;

export { Container, P };