import styled from 'styled-components'

const LiYear = styled.li`
	text-align: center;	
`;

const AYear = styled.a`
	color: white;
	font-size: 70px;
	padding: 0;
`;

const UlYear = styled.ul`
	list-style: none;
`;

export { LiYear, AYear, UlYear };