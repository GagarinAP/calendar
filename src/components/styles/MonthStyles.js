import styled from 'styled-components'

const A = styled.a`
	color: white;
	font-size: 30px;
	padding: 0;
`;

const Ul = styled.ul`
	list-style: none;
`;

const LiCenter = styled.li`
	text-align: center;
	& > span {
	    font-size: 30px;
	}
`;


export { A, Ul, LiCenter };