import React from 'react'
import * as ACTIONS from '../actions/index'
import {withRouter} from 'react-router-dom'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import Nav from './Nav'
import moment from 'moment'
import { LiYear, AYear, UlYear } from './styles/YearStyles'

class Year extends React.Component{
    constructor(props) {
        super(props)

        this.state = {
            defaultDate: moment(this.props.defaultDate),
            events: this.props.events
        }

        this.yearDown = this.yearDown.bind(this)
        this.yearUp = this.yearUp.bind(this)
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.defaultDate !== this.props.defaultDate) {
            this.setState({
                defaultDate: nextProps.defaultDate
            })
        }
    }
    yearDown(number) {
        const { defaultDate } = this.state

        this.setState({
            defaultDate: defaultDate.subtract(number, 'years')
        })

        ACTIONS.defaultDate(defaultDate)
    }
    yearUp(number) {
        const { defaultDate } = this.state

        this.setState({
            defaultDate: defaultDate.add(number, 'years')
        })

        ACTIONS.defaultDate(defaultDate)
    }
    render(){
        const { defaultDate } = this.state

        return(
            <div className="container">
                <div className="row">

                    <Nav defaultDate={defaultDate.format('DD MMMM YYYY')} />

                    <div className="col-md-8 col-md-offset-2">
                        <div className="year">
                            <UlYear>
                                <li className="prev">
                                    <AYear onClick={() => this.yearDown(10)} type="button" className="btn btn-link">
                                        <i className="fa fa-angle-double-left"/>
                                    </AYear>
                                </li>
                                <li className="prev">
                                    <AYear onClick={() => this.yearDown(1)} type="button" className="btn btn-link">
                                        <i className="fa fa-angle-left" />
                                    </AYear>
                                </li>
                                <li className="next">
                                    <AYear onClick={() => this.yearUp(10)} type="button" className="btn btn-link">
                                        <i className="fa fa-angle-double-right" />
                                    </AYear>
                                </li>
                                <li className="next">
                                    <AYear onClick={() => this.yearUp(1)} type="button" className="btn btn-link">
                                        <i className="fa fa-angle-right" />
                                    </AYear>
                                </li>

                                <LiYear>
                                    <span style={{fontSize:'16px'}}>
                                        {(parseInt(defaultDate.format("YYYY"),10) - 2)}
                                    </span>
                                </LiYear>
                                <LiYear>
                                    <span style={{fontSize:'24px'}}>
                                        {(parseInt(defaultDate.format("YYYY"),10) - 1)}
                                    </span>
                                </LiYear>
                                <LiYear>
                                    <span style={{fontSize:'30px'}}>
                                        <strong>{defaultDate.format("YYYY")}</strong>
                                    </span>
                                </LiYear>
                                <LiYear>
                                    <span style={{fontSize:'24px'}}>
                                        {(parseInt(defaultDate.format("YYYY"),10) + 1)}
                                    </span>
                                </LiYear>
                                <LiYear>
                                    <span style={{fontSize:'16px'}}>
                                        {(parseInt(defaultDate.format("YYYY"),10) + 2)}
                                    </span>
                                </LiYear>
                            </UlYear>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    events: state.events,
    defaultDate: state.defaultDate
})

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators(ACTIONS, dispatch)
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Year))