import React from 'react'
import * as ACTIONS from '../actions/index'
import {withRouter} from 'react-router-dom'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import moment from 'moment'
import _ from 'lodash'
import Nav from './Nav'
import uuidv4 from 'uuid/v4'
import { Div, A, LiCenter, Ul } from './styles/DayStyles'

class Day extends React.Component{
    constructor(props) {
        super(props)

        this.state = {
            defaultDate: moment(this.props.defaultDate),
            events: this.props.events
        }

        this.handleChangeDown = this.handleChangeDown.bind(this)
        this.handleChangeUp = this.handleChangeUp.bind(this)
        this.arrayDayInMont = this.arrayDayInMont.bind(this)
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.defaultDate !== this.props.defaultDate) {
            this.setState({
                defaultDate: nextProps.defaultDate
            })
        }
    }

    handleChangeDown() {
        const { defaultDate } = this.state

        this.setState({
            defaultDate: defaultDate.subtract(1, 'day')
        })

        ACTIONS.defaultDate(defaultDate)
    }

    handleChangeUp() {
        const { defaultDate } = this.state

        this.setState({
            date: defaultDate.add(1, 'day')
        })

        ACTIONS.defaultDate(defaultDate)
    }

    arrayDayInMont(events, defaultDate) {
        let days = []

        let result = _.filter(events, todo =>
            moment(todo.date).clone().format('DDMMYYYY') === defaultDate.format('DDMMYYYY')
        )

        for(let i = 0; i < 24; i++){
            days.push(
                <tr key={uuidv4()}>
                    <td>{i}:00</td>
                    <td>
                        {
                            result.map(event => (
                                parseInt(moment(event.date).clone().format('HH'), 10) === i ?
                                    <Div key={uuidv4()}>{moment(event.date).format('HH:mm')} {event.text}</Div> : ''
                            ))
                        }
                    </td>
                </tr>)
        }

        return days
    }

    render(){
        const { events, defaultDate } = this.state

        return(
            <div className="container">
                <div className="row">
                    <Nav defaultDate={defaultDate.format('DD MMMM YYYY')} />
                    <div className="col-md-8 col-md-offset-2">

                        <div className="month">
                            <Ul>
                                <li className="prev">
                                    <A type="button" className="btn btn-link" onClick={this.handleChangeDown}>
                                        <i className="fa fa-angle-left" />
                                    </A>
                                </li>
                                <li className="next">
                                    <A type="button" className="btn btn-link" onClick={this.handleChangeUp}>
                                        <i className="fa fa-angle-right" />
                                    </A>
                                </li>
                                <LiCenter>
                                    <span>
                                        {
                                            defaultDate.format("DD MMMM YYYY")
                                        }
                                    </span>
                                </LiCenter>
                            </Ul>
                        </div>

                        <div className="table-responsive">
                            <table className="table table-hover">
                                <thead>
                                <tr>
                                    <th>Hours</th>
                                    <th>Event(s)</th>
                                </tr>
                                </thead>
                                <tbody>
                                    {
                                        this.arrayDayInMont(events, defaultDate)
                                    }
                                </tbody>
                            </table>
                        </div>

                    </div>

                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    events: state.events,
    defaultDate: state.defaultDate
})

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators(ACTIONS, dispatch)
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Day))