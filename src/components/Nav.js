import React from 'react'
import { Link, NavLink } from 'react-router-dom'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import * as ACTIONS from '../actions/index'
import moment from 'moment'
import { Container, P } from './styles/NavStyles'

class Nav extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            defaultDate: moment(this.props.defaultDate),
            events: this.props.events
        }

        this.handleChangeUp = this.handleChangeUp.bind(this)
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.defaultDate !== this.props.defaultDate || nextProps.events !== this.props.events) {
            this.setState({
                defaultDate: nextProps.defaultDate,
                events: nextProps.events
            })
        }
    }

    handleChangeUp() {
        const date = moment()

        this.setState({
            defaultDate: date.clone()
        })

        ACTIONS.defaultDate(date.clone())
    }

    render() {
        const { events, defaultDate } = this.state

        return (
            <Container className="col-md-12 text-center">

                <h1>Date: {moment(defaultDate).format('DD MMMM YYYY')}</h1>

                <P>Event{events.length > 1 ? "'s:" : ":"}{events.length}</P>

                <div className="btn-group">
                    <Link className="btn btn-lg btn-danger" type="button" to={'/'}>APP</Link>

                    <NavLink className="btn btn-lg btn-primary" type="button" to={'/event'}>
                        <i className="fa fa-plus"/> Event
                    </NavLink>
                    <NavLink className="btn btn-lg btn-danger" type="button" to={'/year'}>Year</NavLink>
                    <NavLink className="btn btn-lg btn-danger" type="button" to={'/month'}>Month</NavLink>
                    <NavLink className="btn btn-lg btn-danger" type="button" to={'/day'}>Day</NavLink>

                    <a onClick={this.handleChangeUp} className="btn btn-lg btn-default" type="button">Today</a>
                </div>

            </Container>
        )
    }
}

const mapStateToProps = (state) => ({
    events: state.events,
    defaultDate: state.defaultDate
})

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators(ACTIONS, dispatch)
})

export default connect(mapStateToProps,mapDispatchToProps)(Nav)