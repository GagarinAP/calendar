# Event calendar

## How to:

1. clone repository
2. install yarn globally (-g) `npm install yarn -g`
3. install dependencies `yarn install`
4. start application `yarn start`
5. http://localhost:3000
